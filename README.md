# termWrite



## Getting started

Send data to STDOUT of another terminal/program by either PID or PTS number.

```
-h | --help     Print this help message.
-p | --pid      Specify the pid to write data to.
-P | --pts      Specify the pseduo terminal to write data to.
-n | --newline  Supress newline character insertion.
-s | --string   String to send.
```

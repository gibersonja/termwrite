package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"strconv"
)

var pid int = -1
var pts int = -1
var newLine bool = true
var data []byte
var err error

func main() {

	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}

		err = scanner.Err()
		er(err)
	}

	if len(os.Args) != 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" || arg == "-h" {
				fmt.Print("-h | --help\tPrint this help message.\n")
				fmt.Print("-p | --pid\tSpecify the pid to write data to.\n")
				fmt.Print("-P | --pts\tSpecify the pseduo terminal to write data to.\n")
				fmt.Print("-n | --newline\tSupress newline character insertion.\n")
				fmt.Print("-s | --string\tString to send.\n")

				return
			}

			if (arg == "-p" || arg == "--pid") && i+1 < len(args) {
				pid, err = strconv.Atoi(args[i+1])
				er(err)
			}

			if (arg == "-P" || arg == "--pts") && i+1 < len(args) {
				pts, err = strconv.Atoi(args[i+1])
				er(err)
			}

			if (arg == "-s" || arg == "--string") && i+1 < len(args) && !isPipe() {
				data = []byte(args[i+1])
			}

			if arg == "-n" || arg == "--newline" {
				newLine = false
			}
		}

		if len(data) == 0 {
			er(fmt.Errorf("Must specify data to send, use -s || --string parameter or pipe from STDIN"))
		}

		if newLine {
			data = append(data, byte('\n'))
		}

		if pid > -1 {
			_, err = os.Stat(fmt.Sprintf("/proc/%d/fd/1", pid))
			er(err)

			err = os.WriteFile(fmt.Sprintf("/proc/%d/fd/1", pid), data, 0220)
			er(err)
		}

		if pts > -1 {
			_, err = os.Stat(fmt.Sprintf("/dev/pts/%d", pts))
			er(err)

			err = os.WriteFile(fmt.Sprintf("/dev/pts/%d", pts), data, 0220)
			er(err)
		}
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
